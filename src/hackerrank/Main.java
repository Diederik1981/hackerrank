package hackerrank;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

//	public static void main(String[] args) {
//
//	}
	
	public static void main(String[] args) {
		System.out.println(nonDivisibleSubset(3, Arrays.asList(1, 7, 2, 4)));
		System.out.println(nonDivisibleSubset(7,
				Arrays.asList(278, 576, 496, 727, 410, 124, 338, 149, 209, 702, 282, 718, 771, 575, 436)));
		
	}

	public static int nonDivisibleSubset(int k, List<Integer> s) {
		int[] count = new int[s.size()];
		for (int x = 0; x < s.size(); x++) {
			for (int y = x + 1; y < s.size(); y++) {
				if ((s.get(x) + s.get(y)) % k != 0) {
					count[x] |= 1 << y;
					count[x] |= 1 << x;
					count[y] |= 1 << x;
					count[y] |= 1 << y;
				}
			}
		}
		Arrays.sort(count);
		List<Integer> sorted = Arrays.stream(count).boxed()
				.sorted(Comparator.comparing(Integer::bitCount).thenComparing(Integer::intValue))
				.collect(Collectors.toList());

		Arrays.sort(count);
		for (int l : sorted) {
			System.out.println(Long.toBinaryString(l));
		}
		System.out.println("----");
		for (int l : count) {
			System.out.println(Long.toBinaryString(l));
		}
		return 0;
	}

}
