package hackerrank.easy;

public class MakingAnagrams {

	public static void main(String[] args) {
		System.out.println(makingAnagrams("a", "z"));
	}
	
	
	
    static int makingAnagrams(String s1, String s2) {

    	int[] arr1 = new int[26];
    	int[] arr2 = new int[26];
    	
    	for (int x = 0; x < s1.length(); x++ ) {
    		int charAt = (int)s1.charAt(x);
    		arr1[charAt - 97]++;
    		System.out.println(charAt);
    	}
    	
    	for (int x = 0; x < s2.length(); x++ ) {
    		int charAt = (int)s2.charAt(x);
    		arr2[charAt - 97]++;
    	}
    	
    	int counter = 0;
    	for (int x = 0; x < arr1.length; x++) {
    		counter += Math.abs(arr1[x] - arr2[x]);
    	}
    	
    	return counter;
    }
}
