package hackerrank.easy;

import java.util.HashSet;
import java.util.Set;

public class StringConstruction {

	public static void main(String[] args) {
		
	}
	
	

    // Complete the stringConstruction function below.
    static int stringConstruction(String s) {
    	Set<Character> chars = new HashSet<>();
    	for (int x =0; x < s.length(); x ++) {
    		chars.add(s.charAt(x));
    	}
    	return chars.size();
    }
}
