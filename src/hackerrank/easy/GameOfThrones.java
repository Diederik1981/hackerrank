package hackerrank.easy;

public class GameOfThrones {

	public static void main(String[] args) {
		System.out.println(gameOfThrones("abbb"));
	}
	
	

    static String gameOfThrones(String s) {
    	int[] vals = new int[26];

    	for (int x = 0; x < s.length(); x++) {
    		vals[s.charAt(x) - 97]++;
    	}
    	
    	int counter = 0;
    	for (int x = 0; x < 26; x++) {
    		counter += vals[x] % 2;
    	}
    	if (counter > 1) {
    		return "NO";
    	}
    	
    	return "YES";
    }

}
