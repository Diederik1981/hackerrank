package hackerrank.easy;

import java.util.HashSet;
import java.util.Set;

public class TwoStrings {

	public static void main(String[] args) {

	}

	// Complete the twoStrings function below.
	static String twoStrings(String s1, String s2) {

		Set<Character> chars1 = new HashSet<>();
		Set<Character> chars2 = new HashSet<>();

		for (int x = 0; x < s1.length(); x++) {
			chars1.add(s1.charAt(x));
		}
		for (int x = 0; x < s2.length(); x++) {
			chars2.add(s2.charAt(x));			
		}

		chars1.retainAll(chars2);
		if (chars1.size() > 0) {
			return "YES";
		} 
		
		return "NO";
	}

}
