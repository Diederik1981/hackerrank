package hackerrank.medium;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SherlockAndValidString {
	public static void main(String[] args) throws IOException {
		
		String string = Files.lines(Paths.get("/development/inputsher.txt")).findFirst().orElse("");
		
		
		System.out.println(isValid(string));
	}

	static String isValid(String s) {
		int[] charcount = new int[26];

		for (int x = 0; x < s.length(); x++) {
			charcount[s.charAt(x) - 97]++;
		}

		int max = 0;
		
		Set<Integer> unique = new HashSet<>();
		for (int x = 0; x < 26; x++) {
			if (charcount[x] != 0) {
				unique.add(charcount[x]);
			}
			System.out.println("x: " + x + " count: " + charcount[x]);
			if (charcount[x] > max) {
				max = charcount[x];
			}
		}
		if(unique.size() > 2) {
			return "NO";
		}
		List<Integer> ints = new ArrayList<>(unique);
		if (ints.size() == 1) {
			return "YES";
			
		}
		if (ints.get(0) == 1 || ints.get(1) == 1) {
			return "YES";
		}
		
		if (Math.abs(ints.get(0) - ints.get(1)) == 1) {
			return "YES";
		}
		

		int maxCount = 0;
		int maxMinus1 = 0;
		int singleCount = 0;
		for (int x = 0; x < 26; x++) {
			int count = charcount[x];
			if (charcount[x] != 0) {
				if (count == max) {
					maxCount++;
				} else if (count == max - 1) {
					maxMinus1++;
				} else {
					if (count == 1 && singleCount == 0 && maxMinus1 == 0) {
						singleCount++;
					}else {
						
						return "NO";
					}
				}
			}
		}
		if (maxMinus1 > 0) {
			if (maxCount > 1) {
				return "NO";
			}
		}
		return "YES";
	}

}
