package hackerrank;

import java.util.Arrays;
import java.util.List;

public class NDSS {
	
	
	
	public static void main(String[] args) {
		ndss(2, Arrays.asList(2,5,4,7));
		ndss(3, Arrays.asList(1,1,1,1));
	}
	
	
	
	static int ndss(int k, List<Integer> arr) {
		System.out.println(arr);
		boolean[][] matrix = new boolean[arr.size()][arr.size()]; 
		
		for (int i = 0; i < arr.size(); i++) {
			for (int j = i + 1; j < arr.size(); j++) {
				if ((i + j) % k != 0) {
					matrix[i][j] = true;
				}
			}
		}
		
		
		for (int i = 0; i < arr.size(); i++) {
			for (int j = 0; j < arr.size(); j++) {
				System.out.print((matrix [i][j] == true ? 1 : 0) + " ");
			}
			System.out.println();
		}
		
		
		return 0;
	}
	
}
